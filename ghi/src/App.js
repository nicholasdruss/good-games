import { BrowserRouter, Routes, Route } from "react-router-dom";
import "./App.css";
import Nav from "./Nav";
import GamesList from "./components/games/GamesList";
import LoginForm from "./components/accounts/Login";
import RegisterForm from "./components/accounts/Register";
import { AuthProvider } from "@galvanize-inc/jwtdown-for-react";
import AccountManagement from "./components/accounts/AccountManagement";
import SearchGame from "./components/games/SearchResults";
import GameDetails from "./components/games/GameDetail";
import Platforms from "./components/platforms/Platforms";
import PlatformDetails from "./components/platforms/PlatformDetails";
import Publishers from "./components/publishers/Publishers";
import PublisherDetails from "./components/publishers/PublisherDetails";

function App() {
  const baseUrl = process.env.REACT_APP_API_HOST;

  return (
    <BrowserRouter>
      <AuthProvider baseUrl={baseUrl}>
        <Nav />
        <div className="container">
          <Routes>
            <Route path="/" element={<GamesList />} />
            <Route path="login" element={<LoginForm />} />
            <Route path="signup" element={<RegisterForm />} />
            <Route path="/game/:id" element={<GameDetails />} />
            <Route path="/account/:id" element={<AccountManagement />} />
            <Route path="/search/:searchQuery" element={<SearchGame />} />
            <Route path="platforms" element={<Platforms />} />
            <Route path="/platform/:platformId" element={<PlatformDetails />} />
            <Route path="publishers" element={<Publishers />} />
            <Route
              path="/publisher/:publisherId"
              element={<PublisherDetails />}
            />
          </Routes>
        </div>
      </AuthProvider>
    </BrowserRouter>
  );
}

export default App;
