import useToken from "@galvanize-inc/jwtdown-for-react";
import { useState, useEffect } from "react";
import icon from "./icon.png";
import { loadReviewByUser } from "../react-functions/LoadData";

function AccountManagement() {
  const { token } = useToken();
  const [isEditMode, setIsEditMode] = useState(false);
  const [editedEmail, setEditedEmail] = useState("");
  const [userData, setUserData] = useState([]);
  const [userId, setUserId] = useState();
  const [userReviews, setUserReviews] = useState([]);
  const [showNotification, setShowNotification] = useState(false);
  const [displayedAvatar, setDisplayedAvatar] = useState(null);
  const [avatarSeed, setAvatarSeed] = useState(
    Math.random().toString(36).substring(7)
  );

  const generateAvatarURL = () => {
    return `https://robohash.org/${avatarSeed}.png`;
  };

  const handleTryAgain = () => {
    const newAvatarSeed = Math.random().toString(36).substring(7);
    setAvatarSeed(newAvatarSeed);
    setDisplayedAvatar(generateAvatarURL(newAvatarSeed));
  };

  function handleImageError(e) {
    e.target.onerror = null;
    e.target.src = icon;
  }

  useEffect(() => {
    if (userData.account) {
      setDisplayedAvatar(userData.account.image_href);
    }
  }, [userData]);

  const handleUpdate = async () => {
    try {
      const updateUrl = `http://localhost:8000/accounts/${userId}`;
      const response = await fetch(updateUrl, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          id: userId,
          email: editedEmail,
          image_href: displayedAvatar,
        }),
      });
      const updatedData = await response.json();
      setUserData(updatedData);
      setIsEditMode(false);
      const fetchUserInfo = async () => {
        const tokenUrl = `http://localhost:8000/token`;
        const response = await fetch(tokenUrl, { credentials: "include" });
        if (response.ok) {
          const data = await response.json();
          setUserData(data);
          setShowNotification(true);
        }
      };
      fetchUserInfo();
    } catch (error) {
      console.error("Error updating account details:", error);
    }
  };

  const handleDelete = async (reviewId) => {
    try {
      const response = await fetch(
        `http://localhost:8000/reviews/${reviewId}`,
        {
          method: "DELETE",
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );

      if (response.ok) {
        setUserReviews((prevReviews) =>
          prevReviews.filter((review) => review.id !== reviewId)
        );
      } else {
        console.error(`Failed to delete review with ID: ${reviewId}`);
      }
    } catch (error) {
      console.error("Error deleting the review:", error);
    }
  };

  useEffect(() => {
    const fetchUserInfo = async () => {
      const tokenUrl = `http://localhost:8000/token`;
      const response = await fetch(tokenUrl, { credentials: "include" });
      if (response.ok) {
        const data = await response.json();
        setUserData(data);
        setUserId(data.account.id);
      }
    };
    fetchUserInfo();
  }, [token]);

  useEffect(() => {
    const fetchUserReviews = async () => {
      try {
        const reviews = await loadReviewByUser(userId);
        setUserReviews(reviews);
      } catch (error) {
        console.error(error);
      }
    };

    if (userId) {
      fetchUserReviews();
    }
  }, [userId]);

  return (
    <div className="container mt-4">
      <div className="row">
        {showNotification && (
          <div className="alert alert-success" role="alert">
            Changes saved! They will be visible on your next login.
          </div>
        )}
        <div className="col-md-6">
          <div className="card mb-4">
            <img
              src={displayedAvatar}
              alt="User Display"
              onError={handleImageError}
              style={{ width: "100px", height: "100px" }}
            />
            <div className="card-body">
              <h1 className="card-title">
                Account - {userData.account?.username}
              </h1>
              <h3>Email</h3>
              {isEditMode ? (
                <div>
                  <input
                    type="text"
                    value={editedEmail}
                    onChange={(e) => setEditedEmail(e.target.value)}
                  />
                  <button onClick={handleTryAgain}>Generate New Avatar</button>
                  <button onClick={handleUpdate}>Save</button>
                </div>
              ) : (
                <div>
                  <p>{userData.account?.email}</p>
                  <button
                    onClick={() => {
                      setIsEditMode(true);
                      setEditedEmail(userData.account?.email);
                    }}
                  >
                    Edit
                  </button>
                </div>
              )}
            </div>
          </div>
        </div>
        <div className="col-md-6">
          <div className="card mb-4">
            <div className="card-body">
              <h1 className="card-title">Your Account Reviews:</h1>
              {userReviews.map((review) => (
                <div key={review.id} className="list-group mb-3">
                  <div className="list-group-item">
                    <h4 key={review.game_id}>{review.game.game_name}</h4>
                    <p>Details: {review.details}</p>
                    <p>Recommended: {review.recommended ? "Yes" : "No"}</p>
                    <button onClick={() => handleDelete(review.id)}>
                      Delete
                    </button>
                  </div>
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
export default AccountManagement;
