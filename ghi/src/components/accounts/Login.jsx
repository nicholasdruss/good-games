import useToken from "@galvanize-inc/jwtdown-for-react";
import { useNavigate } from "react-router-dom";
import { useState, useRef, useEffect } from "react";

const LoginForm = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const { login, token } = useToken();
  const navigate = useNavigate();
  const userRef = useRef();

  const handleSubmit = (e) => {
    e.preventDefault();

    login(username, password);
    e.target.reset();
  };

  useEffect(() => {
    userRef.current.focus();

    if (token) {
      navigate(-1);
    }
  }, [token, navigate]);

  return (
    <>
      <div
        className="card-header text-center d-flex justify-content-center align-items-center"
        style={{ height: "100px" }}
      >
        <div className="bg-base-300">
          <h1 className="text-primary-focus text-5xl font-bold text-center pt-2">
            Welcome back!
          </h1>
        </div>
      </div>
      <div className="hero min-h-screen bg-base-300">
        <div className="hero-content flex-col lg:flex-row-reverse">
          <div className="lg:text-left text-base-content"></div>
          <div className="card flex-shrink-0 w-full max-w-sm shadow-2xl bg-base-200">
            <form onSubmit={handleSubmit}>
              <div className="card-body p-5">
                <ul
                  class="nav nav-pills nav-justified mb-3"
                  id="ex1"
                  role="tablist"
                >
                  <li class="nav-item" role="presentation">
                    <a
                      class="nav-link active"
                      id="tab-login"
                      data-toggle="pill"
                      href="/login"
                      role="tab"
                      aria-controls="pills-login"
                      aria-selected="true"
                    >
                      Login
                    </a>
                  </li>
                  <li class="nav-item" role="presentation">
                    <a
                      class="nav-link"
                      id="tab-register"
                      data-toggle="pill"
                      href="/signup"
                      role="tab"
                      aria-controls="pills-register"
                      aria-selected="false"
                      style={{ backgroundColor: "rgba(230, 240, 254)" }}
                    >
                      Register
                    </a>
                  </li>
                </ul>
                <div className="bg-base-200">
                  <label className="label text-base-content">Username</label>
                  <input
                    type="text"
                    ref={userRef}
                    name="username"
                    className="form-control input input-bordered"
                    onChange={(e) => setUsername(e.target.value)}
                  />
                </div>
                <div className="bg-base-200">
                  <label className="label text-base-content text-lg">
                    Password
                  </label>
                  <input
                    name="password"
                    type="password"
                    onChange={(e) => setPassword(e.target.value)}
                    className="input input-bordered form-control"
                  />
                </div>
                <div className="mt-4 bg-base-200 flex justify-center">
                  <button
                    className="btn btn-primary btn-wide"
                    type="submit"
                    value="login"
                  >
                    Sign In
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </>
  );
};

export default LoginForm;
