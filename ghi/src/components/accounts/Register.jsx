import { useState } from "react";
import useToken from "@galvanize-inc/jwtdown-for-react";
import { useNavigate } from "react-router-dom";

const RegisterForm = () => {
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const { register } = useToken();
  const navigate = useNavigate();

  const handleSubmit = (e) => {
    e.preventDefault();

    const userData = {
      username: username,
      email: email,
      image_href: "",
      password: password,
    };

    register(userData, "http://localhost:8000/accounts");
    e.target.reset();
    navigate("/");
  };

  return (
    <>
      <div className="card-header" style={{ height: "100px" }}>
        <div className="bg-base-300">
          <h1 className="text-primary-focus text-5xl font-bold text-center pt-2">
            Join us!
          </h1>
          <p className="pb-4 pt-1 text-secondary text-center text-black">
            Your favorite game is one review away...
          </p>
        </div>
      </div>
      <div className="hero bg-base-300">
        <div className="hero-content flex-col lg:flex-row-reverse">
          <div className="lg:text-left text-base-content"></div>

          <div className="card flex-shrink-0 w-full max-w-sm shadow-2xl bg-base-200">
            <form onSubmit={handleSubmit}>
              <div className="card-body bg-base-200  p-5">
                <ul
                  class="nav nav-pills nav-justified mb-3"
                  id="ex1"
                  role="tablist"
                >
                  <li class="nav-item" role="presentation">
                    <a
                      class="nav-link"
                      id="tab-login"
                      data-mdb-toggle="pill"
                      href="/login"
                      role="tab"
                      aria-controls="pills-login"
                      aria-selected="false"
                      style={{ backgroundColor: "rgba(230, 240, 254)" }}
                    >
                      Login
                    </a>
                  </li>
                  <li class="nav-item" role="presentation">
                    <a
                      class="nav-link active"
                      id="tab-register"
                      data-mdb-toggle="pill"
                      href="/signup"
                      role="tab"
                      aria-controls="pills-register"
                      aria-selected="true"
                    >
                      Register
                    </a>
                  </li>
                </ul>
                <div className="bg-base-200">
                  <label className="label text-base-content mt-0">Email</label>
                  <input
                    type="email"
                    id="email"
                    name="email"
                    value={email}
                    className="form-control input input-bordered"
                    onChange={(e) => setEmail(e.target.value)}
                    required
                  />
                </div>

                <div className="bg-base-200">
                  <label className="label text-base-content mt-0">
                    Username
                  </label>
                  <input
                    type="text"
                    id="username"
                    name="username"
                    value={username}
                    className="form-control input input-bordered"
                    onChange={(e) => setUsername(e.target.value)}
                    required
                    aria-describedby="uidnote"
                  />
                </div>

                <div className="bg-base-200">
                  <label className="label text-base-content mt-0">
                    Password
                  </label>
                  <input
                    type="password"
                    id="password"
                    name="password"
                    value={password}
                    className="form-control input input-bordered"
                    onChange={(e) => setPassword(e.target.value)}
                    autoComplete="off"
                    required
                  />
                </div>

                <div className="mt-4 bg-base-200 flex justify-center">
                  <button
                    className="btn btn-primary btn-wide"
                    type="submit"
                    value="Register"
                  >
                    Sign In
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </>
  );
};
export default RegisterForm;
