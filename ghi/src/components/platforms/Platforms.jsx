import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import icon from "./icon.png";

function Platforms() {
	const [platforms, setPlatforms] = useState([]);

	async function loadPlatforms() {
		const url = "http://localhost:8000/platform";
		const response = await fetch(url);
		if (response.ok) {
			const data = await response.json();
			setPlatforms(data);
		}
	}

	useEffect(() => {
		loadPlatforms();
	}, []);

	return (
		<div className="container">
			<h1>Platforms</h1>
			<div className="row">
				{platforms.map((platform) => (
					<div
						key={platform.id}
						className="col-12 col-sm-6 col-md-4 col-lg-3"
					>
						<div className="card mb-4">
							<img
								src={platform.image_href ? platform.image_href : icon}
								className="card-img-top platform-image"
								alt={platform.platform_name}
							/>
							<div className="card-body">
								<h5 className="card-title">{platform.platform_name}</h5>
								<Link
									to={`/platform/${platform.id}`}
									className="btn btn-primary"
								>
									View Details
								</Link>
							</div>
						</div>
					</div>
				))}
			</div>
		</div>
	);
}

export default Platforms;
