import React, { useEffect } from "react";
import { useLocation, useParams } from "react-router-dom";
import { useState } from "react";

function SearchGame() {
	const { searchQuery } = useParams();
	const { state: results } = useLocation();
	const [searchData, setSearchData] = useState([]);

	useEffect(() => {
		const loadSearchResults = async () => {
			const response = await fetch(
				`http://localhost:8000/search/${searchQuery}`
			);
			if (response.ok) {
				const data = await response.json();
				setSearchData(data);
			}
		};
		loadSearchResults(searchQuery);
	}, [searchQuery, results]);

	return (
		<>
			<div className="row">
				{searchData.map((game) => (
					<div
						key={game.id}
						className="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2 mb-3 mx-5"
					>
						<a
							key={game.id}
							href={`/game/${game.id}`}
							className="game-card-link"
						>
							<div
								key={game.id}
								className="card game-card"
								style={{ width: "18rem" }}
							>
								<img
									src={game.image_href}
									className="card-img-top game-card-image"
									alt="..."
								/>
								<div className="card-body">
									<h5 className="card-title">{game.game_name}</h5>
								</div>
							</div>
						</a>
					</div>
				))}
			</div>
		</>
	);
}

export default SearchGame;
