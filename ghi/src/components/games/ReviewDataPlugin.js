import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { ReviewDataForGame } from "../react-functions/MadeReviews";

function GameReviewData() {
  const { id } = useParams();
  const [reviewData, setReviewData] = useState(null);

  useEffect(() => {
    async function fetchData() {
      const data = await ReviewDataForGame(id);
      setReviewData(data);
    }

    fetchData();
  }, [id]);

  if (!reviewData) {
    return <div>Review Data Loading...</div>;
  }

  return (
    <div className="card mt-4">
      <div className="card-body">
        <h2 className="card-title">Game Review Data</h2>
        <p>Total Reviews: {reviewData.totalReviews}</p>
        <p>Like Percentage: {reviewData.likePercentage}%</p>
        <p>Dislike Percentage: {reviewData.dislikePercentage}%</p>
        <p>Overall Percentage: {reviewData.overallPercentage}%</p>
      </div>
    </div>
  );
}

export default GameReviewData;
