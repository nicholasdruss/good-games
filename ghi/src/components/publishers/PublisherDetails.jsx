import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";

function PublisherDetails() {
	const { publisherId } = useParams();
	const [publisherDetails, setPublisherDetails] = useState({});

	useEffect(() => {
		async function fetchPublisherDetails() {
			const url = `http://localhost:8000/publisher/${publisherId}`;
			const response = await fetch(url);
			if (response.ok) {
				const data = await response.json();
				setPublisherDetails(data);
			}
		}

		fetchPublisherDetails();
	}, [publisherId]);

	return (
		<div className="container">
			<div className="card mt-4">
				<img
					src={publisherDetails.image_href}
					className="card-img-top img short-image"
					alt={publisherDetails.publisher_name}
				/>
				<div className="card-body">
					<h5 className="card-title">{publisherDetails.name}</h5>
					{publisherDetails.games_count && (
						<p>Number of Games: {publisherDetails.games_count}</p>
					)}
					<div
						className="description-content"
						dangerouslySetInnerHTML={{ __html: publisherDetails.description }}
					></div>
				</div>
			</div>
		</div>
	);
}

export default PublisherDetails;
