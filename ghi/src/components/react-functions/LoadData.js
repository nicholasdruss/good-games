export async function loadReviewByGame(id) {
  const reviewUrl = `http://localhost:8000/reviews/game/${id}`;
  const response = await fetch(reviewUrl);
  if (response.ok) {
    const data = await response.json();
    return data;
  }
}

export async function loadReviewByUser(id) {
  try {
    const reviewUrl = `http://localhost:8000/reviews/user/${id}`;
    const response = await fetch(reviewUrl);
    if (response.status === 405) {
      return [];
    }
    if (response.ok) {
      const data = await response.json();
      return data;
    } else {
      throw new Error("Failed to fetch reviews.");
    }
  } catch (error) {
    console.error("An error occurred:", error);
    return [];
  }
}

export async function loadGameDetailData(id) {
  const fetchUrl = `http://localhost:8000/game/${id}`;
  const response = await fetch(fetchUrl);
  if (response.ok) {
    const data = await response.json();
    return data;
  }
}

export async function loadCheckGameData(id) {
  const fetchUrl = `http://localhost:8000/checkgame/${id}`;
  const response = await fetch(fetchUrl);
  if (response.ok) {
    const data = await response.json();
    return data;
  }
}

export async function loadUserInfo() {
  const tokenUrl = "http://localhost:8000/token";
  const response = await fetch(tokenUrl, { credentials: "include" });
  if (response.ok) {
    const data = await response.json();
    return data;
  }
}

export async function loadSearchResults(search) {
  const fetchUrl = `http://localhost:8000/search/${search}`;
  const response = await fetch(fetchUrl);
  if (response.ok) {
    const data = await response.json();
    return data;
  }
}
