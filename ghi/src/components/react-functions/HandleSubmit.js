async function addGameToDatabase(reviewData, gameData, token, gameId) {
  try {
    const gUrl = "http://localhost:8000/game/add";
    const fetchGameConfig = {
      method: "post",
      body: JSON.stringify(gameData),
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
    };

    const addGameResponse = await fetch(gUrl, fetchGameConfig);
    if (addGameResponse.ok) {
      createReview(reviewData, gameData, token, gameId);
    }
  } catch (error) {
    console.error("Couldnt add game to database:", error);
  }
}

export async function createReview(reviewData, gameData, token, gameId) {
  const checkUrl = `http://localhost:8000/checkgame/${gameId}`;
  try {
    const checkResponse = await fetch(checkUrl);
    const response = await checkResponse.json();
    if (response.message && response.message === "game does not exist") {
      addGameToDatabase(reviewData, gameData, token, gameId);
    } else {
      const rUrl = "http://localhost:8000/reviews";
      const fetchReviewConfig = {
        method: "post",
        body: JSON.stringify(reviewData),
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`,
        },
      };

      const reviewResponse = await fetch(rUrl, fetchReviewConfig);
      if (reviewResponse.ok) {
        window.location.reload();
      }
    }
  } catch (error) {
    console.error("Couldnt add game to database:", error);
  }
}
