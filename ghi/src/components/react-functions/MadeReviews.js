import { loadReviewByUser, loadReviewByGame } from "./LoadData";

export async function Reviewed(userId, game_id) {
  try {
    const reviewByUser = await loadReviewByUser(userId);
    const gameIdNum = Number(game_id);

    for (let review of reviewByUser) {
      if (review.game_id === gameIdNum) {
        return false;
      }
    }

    return true;
  } catch (error) {
    console.error(error);
    throw error;
  }
}

export async function ReviewDataForGame(game_id) {
  try {
    const reviewByGame = await loadReviewByGame(game_id);
    let reviewCount = 0;
    let recommended = 0;
    let notRecommended = 0;

    if (reviewByGame.length === 0) {
      return {
        totalReviews: reviewCount,
        likePercentage: 0,
        dislikePercentage: 0,
        overallPercentage: 0,
      };
    }

    for (let review of reviewByGame) {
      if (review) {
        reviewCount += 1;
        if (review.recommended === true) {
          recommended += 1;
        } else {
          notRecommended += 1;
        }
      }
    }

    const likePercentage = ((recommended / reviewCount) * 100).toFixed(2);
    const dislikePercentage = ((notRecommended / reviewCount) * 100).toFixed(2);

    const overallPercentage = (100 - dislikePercentage).toFixed(2);

    return {
      totalReviews: reviewCount,
      likePercentage: parseFloat(likePercentage),
      dislikePercentage: parseFloat(dislikePercentage),
      overallPercentage: parseFloat(overallPercentage),
    };
  } catch (error) {
    console.error(error);
  }
}
