import { NavLink, useNavigate } from "react-router-dom";
import React, { useState, useEffect } from "react";
import useToken from "@galvanize-inc/jwtdown-for-react";

function Nav() {
  const [search, setSearch] = useState("");
  const [userId, setUserId] = useState();
  const { logout, token } = useToken();
  const navigate = useNavigate();

  useEffect(() => {
    const fetchUserInfo = async () => {
      const tokenUrl = `http://localhost:8000/token`;
      const response = await fetch(tokenUrl, { credentials: "include" });
      if (response.ok) {
        const data = await response.json();
        setUserId(data.account.id);
      }
    };
    fetchUserInfo();
  }, [token]);

  const handleSearch = (e) => {
    e.preventDefault();
    navigate(`/search/${search}`);
    setSearch("");
  };

  return (
    <nav
      style={{ color: "#C06014" }}
      className="navbar navbar-expand-lg bg-body-tertiary"
    >
      <div className="container-fluid">
        <a className="navbar-brand" href="/">
          Good Games
        </a>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <a className="nav-link active" aria-current="page" href="/">
                Home
              </a>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/platforms">
                Platforms
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/publishers">
                Publishers
              </NavLink>
            </li>
          </ul>
        </div>
        {!token ? (
          <>
            <NavLink className="text-decoration-none" to="/login">
              <ul className="navbar-nav me-auto mb-2 mb-lg-0 d-flex">
                <li className="nav-link">
                  Log In/Sign Up
                </li>
              </ul>
            </NavLink>

          </>
        ) : (
          <>
            <NavLink className="text-decoration-none" to={`/account/${userId}`}>
              <ul className="navbar-nav me-auto mb-2 mb-lg-0 d-flex">
                <li className="nav-link">
                  Account Management
                </li>
              </ul>
            </NavLink>
            <NavLink className="text-decoration-none" to="/">
              <ul className="navbar-nav me-auto mb-2 mb-lg-0 d-flex">
                <li className="nav-link" onClick={logout}>
                  Logout
                </li>
              </ul>
            </NavLink>
          </>
        )}
        <form className="d-flex" onSubmit={handleSearch}>
          <input
            className="form-control me-2"
            type="search"
            placeholder="Search for a game"
            aria-label="Search"
            value={search}
            onChange={(e) => setSearch(e.target.value)}
          />
          <button className="btn btn-primary" type="submit">
            Search!
          </button>
        </form>
      </div>
    </nav>
  );
}

export default Nav;
