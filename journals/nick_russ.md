Week 1
08/07/2023
We made strides today to discuss ideals and settle on an item. Everyone seemed content with our group dynamic. We decided to creating a gaming review website and proceeded to build the wire framework and diagram.

08/08/2023
Today the team focused on catching up on lessons and discoveries in prep. We are confident in our ideal still and ready to start. Tomorrow we will begin delving in.

08/09/2023
Today we worked on basic DB setup and getting the start going with our code. Solved and performed troubleshooting on issues to get everyone on the same page.

08/10/2023
Today we continued work on wireframing, Error handling, and continuing to flesh out project plans. Began research on PostgreSQL table setup.

08/11/2023
Continuing work and making progress on DB schema file to create my tables.

Week 2
08/14/2023
Today we refreshed our project and prepped to started resetting to try and get a more grounded start with the new skills we had learned. I was able to fully implement our PostgreSQL database for all the needed tables.

08/15/2023
Today we began working on our backend endpoints. Development continues along at a decent pace.

08/16/2023

08/17/2023

08/18/2023
Day off

Week 3
08/21/2023
I did some stuff

08/22/2023
I did some stuff

08/23/2023
I did some stuff

08/24/2023
I did some stuff

08/25/2023
I did some stuff

Week 4
08/28/2023
Had to go back to tables and work on issues relating to table linking, and added in additional details.

08/29/2023
I did some stuff

08/30/2023
I did some stuff

08/31/2023
I did some stuff

09/01/2023
Day off Spent this weekend working on Game Details. Spent time refactoring my code and attempting to make it cleaner.

Week 5
09/04/2023
Labor day Day off

09/05/2023
Continued work on Game Details.

09/06/2023

09/07/2023
Fixed the final issues with game details. Wrote a function that will check and see if the user has written a review and set whether they are allowed to write a review. This fixed the functionality with the review allowing a user to repost more than one review by refreshing the page.

09/08/2023
Today was about finishing implementation of functionality and refactoring finality of plans. We had a discussion about what is actually stretch goal and what can still be implemented before deadline. Also set about prepping for weekend plans of what has to get finished. Assigned CSS duties to our late project addition. Finally, built a query to create a mock foreign key for use in reviews to populate the game name data. This was satisfying to have successfully work allowing me to post the game name and have access to the other details for use on the account management page.

09/09/2023
Finished search bar functionality with testing. Built a function and plugin for handling calculating total reviews for a given game, and calculating percentages of negative to positive reviews with total review percentage.

09/10/2023
Finished unit test for checkgame functionality. Added some CSS customization. Began merging all working branches into main and testing backend endpoints and front end for responsiveness.

Week 6
09/11/2023
Project Submit date is today. Working on testing errors and plan to work on CI/CD planning and implementation today.
