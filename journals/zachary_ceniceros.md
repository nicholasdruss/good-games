Week 1:

    08/07/2023:
    talked as a team and figured out group roles. decided to create good games, a video game review website. built out wireframe in excalidraw

    08/08/2023:
    caught up in coursework

    08/09/2023:
    started db setup and did some troubleshooting for each other in regards to our DBs

    08/10/2023:
    continued to flesh out project by working on deeper detail

    08/11/2023:
    continued to flesh out project by working on deeper detail + db setup

----------

Week 2:

    08/14/2023:
    rebuilt project in a new repo to start off fresh with our newly-found knowledge

    08/15/2023:
    backend endpoints began being built out

    08/16/2023:
    backend endpoints

    08/17/2023:
    backend endpoints

    08/18/2023:
    day off

----------

Week 3:

    08/21/2023:
    began fleshing out some frontend design, creating mock data to visually see how actual data will look in browser

    08/22/2023:
    worked more in frontend design

    08/23/2023:
    worked more in frontend design

    08/24/2023:
    worked more in frontend design

    08/25/2023:
    worked more in frontend design

----------

Week 4:

    08/28/2023:
    backend team had to work on fixing tables, continued to flesh out frontend design

    08/29/2023:
    worked more in frontend design

    08/30/2023:
    created homepage for frontend [it's my bday :P]

    08/31/2023:
    added react router to frontend, as well as started using jwtdown for authentication in frontend

    09/01/2023:
    frontend authentication completed / day off

----------

Week 5:

    09/04/2023:
    labor day / day off

    09/05/2023:
    worked on account detail page

    09/06/2023:
    continued to work on account detail page

    09/07/2023:


    09/08/2023:


    09/09/2023:


    09/10/2023:

----------

Week 6:

    09/11/2023:
    project due date is today! i believe the team is going to try and start deployment
