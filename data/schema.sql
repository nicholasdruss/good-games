DROP TABLE IF EXISTS reviews;
DROP TABLE IF EXISTS details;
DROP TABLE IF EXISTS games;
DROP TABLE IF EXISTS accounts;

CREATE TABLE IF NOT EXISTS accounts(
    id SERIAL NOT NULL UNIQUE,
    email VARCHAR(255) NOT NULL UNIQUE,
    username VARCHAR(255) NOT NULL UNIQUE,
    image_href TEXT,
    hashed_password VARCHAR(255) NOT NULL,
    account_created DATE NOT NULL DEFAULT CURRENT_DATE,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS games(
    id SERIAL NOT NULL UNIQUE,
    game_id INT NOT NULL UNIQUE,
    image_href TEXT,
    game_name VARCHAR(255),
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS reviews(
    id SERIAL NOT NULL UNIQUE,
    game_id INT NOT NULL REFERENCES games(game_id) ON DELETE CASCADE,
    user_id INT NOT NULL REFERENCES accounts(id) ON DELETE CASCADE,
    username VARCHAR(255) NOT NULL REFERENCES accounts(username) ON DELETE CASCADE,
    recommended BOOLEAN NOT NULL,
    details TEXT,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS details(
    id SERIAL NOT NULL UNIQUE,
    game_id INT NOT NULL REFERENCES games(id) ON DELETE CASCADE,
    game_name VARCHAR(255),
    details TEXT,
    image_href TEXT,
    release_date VARCHAR(255),
    platforms VARCHAR(255), 
    PRIMARY KEY (id)
);

SELECT setval('accounts_id_seq', (SELECT MAX(id) + 1 FROM accounts));
SELECT setval('games_id_seq', (SELECT MAX(id) + 1 FROM games));
SELECT setval('reviews_id_seq', (SELECT MAX(id) + 1 FROM reviews));
SELECT setval('details_id_seq', (SELECT MAX(id) + 1 FROM details));
