from fastapi import Depends, APIRouter, HTTPException
from typing import List
from queries.reviews import (
    ReviewQueries,
    ReviewIn,
    ReviewOut,
    ReviewOutWithGame,
)
from queries.games import GameQueries
from authenticator import authenticator

router = APIRouter()


@router.get("/reviews", response_model=List[ReviewOut])
def get_all_reviews(query: ReviewQueries = Depends(ReviewQueries)):
    try:
        result = query.get_all_reviews()
        return result
    except Exception:
        raise HTTPException(
            status_code=500,
            detail="Could not list all reviews",
        )


@router.post("/reviews", response_model=ReviewOut)
def add_review(
    review: ReviewIn,
    account_data: dict = Depends(authenticator.get_current_account_data),
    query: ReviewQueries = Depends(),
):
    try:
        result = query.add_review(
            review.game_id,
            review.user_id,
            review.username,
            review.recommended,
            review.details,
        )
        return result
    except Exception as e:
        raise HTTPException(
            print(e),
            status_code=500,
            detail="Could not create review",
        )


@router.get("/reviews/user/{user_id}", response_model=List[ReviewOutWithGame])
def get_reviews_by_user_id(
    user_id: int,
    query: ReviewQueries = Depends(),
    game_query: GameQueries = Depends(),
):
    try:
        result = query.get_reviews_by_user_with_game(user_id)
        if not result:
            return []
        game_ids = set(review.game_id for review in result)
        game_details = {}
        for game_id in game_ids:
            game = game_query.get_game(game_id)
            if game:
                game_details[game_id] = game
        for review in result:
            game_id = review.game_id
            if game_id in game_details:
                review.game = game_details[game_id]
        return result
    except Exception:
        raise HTTPException(
            status_code=500,
            detail="Could not retrieve reviews for specified user id",
        )


@router.get("/reviews/game/{game_id}", response_model=List[ReviewOut])
def get_reviews_by_game(
    game_id: int, query: ReviewQueries = Depends(ReviewQueries)
):
    try:
        result = query.get_reviews(game_id)
        return result
    except Exception:
        raise HTTPException(
            status_code=500,
            detail="Could not retrieve reviews for specified game id",
        )


@router.delete("/reviews/{id}", response_model=bool)
def delete_review(
    id: int,
    query: ReviewQueries = Depends(),
):
    try:
        result = query.delete_review(id)
        return result
    except Exception:
        raise HTTPException(
            status_code=500,
            detail="Could not delete review",
        )
