from fastapi import APIRouter, HTTPException
from typing import List, Optional
from keys import RAWG_API_KEY
import httpx
from pydantic import BaseModel

router = APIRouter()


class PlatformsIn(BaseModel):
    id: int
    platform_name: Optional[str]
    image_href: Optional[str]


class PlatformIn(BaseModel):
    id: int
    platform_name: Optional[str]
    image_href: Optional[str]
    games_count: Optional[int]
    image: Optional[str]
    description: Optional[str]
    year_start: Optional[str]
    year_end: Optional[str]


@router.get("/platform", response_model=List[PlatformsIn])
async def get_platform_list():
    url = "https://api.rawg.io/api/platforms"
    params = {"key": RAWG_API_KEY}

    try:
        async with httpx.AsyncClient() as client:
            response = await client.get(url, params=params)
            response.raise_for_status()

            data = response.json()
            results = data.get("results", [])

            platforms = [
                PlatformsIn(
                    id=platform["id"],
                    platform_name=platform["name"],
                    image_href=platform["image_background"],
                )
                for platform in results
            ][:18]

            return platforms
    except httpx.HTTPError:
        raise HTTPException(
            status_code=500,
            detail="Could not retrieve list of platforms",
        )


@router.get("/platform/{platform_id}", response_model=PlatformIn)
async def get_platform(platform_id: int):
    url = f"https://api.rawg.io/api/platforms/{platform_id}"
    params = {"key": RAWG_API_KEY}

    try:
        async with httpx.AsyncClient() as client:
            response = await client.get(url, params=params)
            response.raise_for_status()

            data = response.json()

            platform = PlatformIn(
                id=data["id"],
                platform_name=data["name"],
                games_count=data["games_count"],
                image_href=data["image_background"],
                image=data["image"],
                description=data["description"],
                year_start=data["year_start"],
                year_end=data["year_end"],
            )

            return platform
    except httpx.HTTPError:
        raise HTTPException(
            status_code=500,
            detail="Could not retrieve platform by id",
        )
