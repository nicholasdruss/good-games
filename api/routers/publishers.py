from fastapi import APIRouter, HTTPException
from typing import List, Optional
from keys import RAWG_API_KEY
import httpx
from pydantic import BaseModel

router = APIRouter()


class PublishersIn(BaseModel):
    id: int
    name: str
    image_href: Optional[str]


class PublisherIn(BaseModel):
    id: int
    name: Optional[str]
    games_count: Optional[int]
    image_href: Optional[str]
    description: Optional[str]


@router.get("/publishers", response_model=List[PublishersIn])
async def get_publisher_list():
    url = "https://api.rawg.io/api/publishers"
    params = {"key": RAWG_API_KEY}

    try:
        async with httpx.AsyncClient() as client:
            response = await client.get(url, params=params)
            response.raise_for_status()
            data = response.json()
            results = data.get("results", [])

            publishers = [
                PublishersIn(
                    id=publisher["id"],
                    name=publisher["name"],
                    image_href=publisher.get("image_background"),
                )
                for publisher in results
            ]

            return publishers
    except httpx.HTTPError:
        raise HTTPException(
            status_code=500,
            detail="Could not retrieve list of publishers",
        )


@router.get("/publisher/{publisher_id}", response_model=PublisherIn)
async def get_publisher(publisher_id: int):
    url = f"https://api.rawg.io/api/publishers/{publisher_id}"
    params = {"key": RAWG_API_KEY}

    try:
        async with httpx.AsyncClient() as client:
            response = await client.get(url, params=params)
            response.raise_for_status()
            data = response.json()

            publisher = PublisherIn(
                id=data["id"],
                name=data["name"],
                image_href=data["image_background"],
                games_count=data["games_count"],
                description=data["description"],
            )

            return publisher
    except httpx.HTTPError:
        raise HTTPException(
            status_code=500,
            detail="Could not retrieve publisher by specified id",
        )
