from jwtdown_fastapi.authentication import Token
from authenticator import authenticator
from pydantic import BaseModel, ValidationError
from typing import Optional
from fastapi import (
    Depends,
    APIRouter,
    Request,
    Response,
    HTTPException,
    status,
)
from queries.accounts import (
    AccountIn,
    AccountOut,
    AccountQueries,
    DuplicateAccountError,
    AccountUpdate
)

router = APIRouter()


class AccountForm(BaseModel):
    email: str
    username: str
    image_href: str
    password: str


class AccountToken(Token):
    account: AccountOut


@router.post("/accounts", response_model=AccountToken)
async def create_account(
    info: AccountIn,
    request: Request,
    response: Response,
    accounts: AccountQueries = Depends(),
):
    try:
        hashed_password = authenticator.hash_password(info.password)
        account = accounts.create(info, hashed_password)
        form = AccountForm(
            email=info.email,
            username=info.username,
            image_href=info.image_href,
            password=info.password,
        )
        token = await authenticator.login(response, request, form, accounts)
        return AccountToken(account=account, **token.dict())
    except DuplicateAccountError:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="An account with this email already exists.",
        )
    except ValidationError:
        raise HTTPException(
            status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
            detail="Invalid account information. Please check your input",
        )
    except Exception:
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail="An error occurred while creating the account.",
        )


@router.get("/token", response_model=AccountToken)
async def get_token(
    request: Request,
    account: AccountIn = Depends(authenticator.try_get_current_account_data),
) -> AccountToken:
    try:
        if account and authenticator.cookie_name in request.cookies:
            return {
                "access_token": request.cookies[authenticator.cookie_name],
                "type": "Bearer",
                "account": account,
            }
        else:
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED,
                detail="Unauthorized or missing credentials.",
            )
    except Exception:
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail="An error occurred while processing the token request.",
        )


@router.get("/accounts/{id}", response_model=AccountOut)
async def get_account(
    request: Request,
    id: int,
    query: AccountQueries = Depends(),
    account_data: Optional[dict] = Depends(),
) -> AccountOut:
    try:
        return query.get_account(id)
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=e
        )


@router.put("/accounts/{id}", response_model=AccountUpdate)
async def update_account(
    id: int,
    account_update: AccountUpdate,
    accounts: AccountQueries = Depends(),
):
    try:
        updated_account = accounts.update_account(id, account_update)

        if not updated_account:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail=f"Account with ID {id} not found.",
            )

        return updated_account

    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=e
        )
