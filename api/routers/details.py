from fastapi import APIRouter, HTTPException
from pydantic import BaseModel
import httpx
from typing import List
from keys import RAWG_API_KEY

router = APIRouter()


class Details(BaseModel):
    id: int
    game_name: str
    details: str
    image_href: str
    release_date: str
    platforms: List[str]


@router.get("/game/{game_id}", response_model=Details)
async def get_game_details(game_id: int):
    url = f"https://api.rawg.io/api/games/{game_id}"
    params = {"key": RAWG_API_KEY}

    try:
        async with httpx.AsyncClient() as client:
            response = await client.get(url, params=params)
            response.raise_for_status()

            data = response.json()

            details = Details(
                id=data["id"],
                game_name=data["name"],
                details=data["description"],
                image_href=data["background_image"],
                release_date=data["released"],
                platforms=[
                    platform["platform"]["name"]
                    for platform in data["metacritic_platforms"]
                ],
            )

            return details
    except httpx.HTTPError:
        raise HTTPException(
            status_code=500, detail="Could not retrieve game details"
        )
