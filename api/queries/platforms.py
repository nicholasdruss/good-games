from pydantic import BaseModel
from psycopg_pool import ConnectionPool
import os

pool = ConnectionPool(conninfo=os.environ.get("DATABASE_URL"))


class DuplicateAccountError(ValueError):
    pass


class PlatformIn(BaseModel):
    platform_id: int
    platform_name: str
    image_href: str


class PlatformOut(BaseModel):
    id: int
    platform_id: int
    platform_name: str
    image_href: str


class PlatformQueries:
    def get_platform(self, platform_id) -> PlatformOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id, platform_id, platform_name, image_href
                        FROM platforms
                        WHERE platform_id = %s;
                        """,
                        [platform_id],
                    )
                    record = result.fetchone()
                    if record is None:
                        return None
                    return PlatformOut(
                        id=int(record[0]),
                        platform_id=record[1],
                        platform_name=record[2],
                        image_href=record[3],
                    )
        except Exception as e:
            return {"message": "Could not retrieve platform", "error": e}
