from pydantic import BaseModel
from psycopg_pool import ConnectionPool
import os
from typing import Optional

pool = ConnectionPool(conninfo=os.environ.get("DATABASE_URL"))


class DuplicateAccountError(ValueError):
    pass


class AccountIn(BaseModel):
    email: str
    username: str
    image_href: str
    password: str


class AccountOut(BaseModel):
    id: int
    email: str
    username: str
    image_href: str


class AccountUpdate(BaseModel):
    email: Optional[str] = None
    image_href: Optional[str] = None


class AccountOutWithPassword(AccountOut):
    hashed_password: str


class AccountQueries:
    def get_account(self, username) -> AccountOutWithPassword:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id, email, username, image_href, hashed_password
                        FROM accounts
                        WHERE username = %s;
                        """,
                        [username],
                    )
                    record = result.fetchone()
                    if record is None:
                        return None
                    return AccountOutWithPassword(
                        id=int(record[0]),
                        email=record[1],
                        username=record[2],
                        image_href=record[3],
                        hashed_password=record[4],
                    )
        except Exception as e:
            return {"message": e}

    def get_account_by_id(self, id) -> AccountOutWithPassword:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id, email, username, image_href, hashed_password
                        FROM accounts
                        WHERE id = %s;
                        """,
                        [id],
                    )
                    record = result.fetchone()
                    if record is None:
                        return None
                    return AccountOutWithPassword(
                        id=int(record[0]),
                        email=record[1],
                        username=record[2],
                        image_href=record[3],
                        hashed_password=record[4],
                    )
        except Exception as e:
            return {"messsage": e}

    def create(
        self, account: AccountIn, hashed_password: str
    ) -> AccountOutWithPassword:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO accounts (
                            email,
                            username,
                            image_href,
                            hashed_password
                            )
                        VALUES (%s, %s, %s, %s)
                        RETURNING id;
                        """,
                        [
                            account.email,
                            account.username,
                            account.image_href,
                            hashed_password,
                        ],
                    )
                    id = result.fetchone()[0]
                    return AccountOutWithPassword(
                        id=id,
                        email=account.email,
                        hashed_password=hashed_password,
                        username=account.username,
                        image_href=account.image_href,
                    )
        except Exception as e:
            return {"message": e}

    def update_account(
        self, account_id: int, account: AccountUpdate
    ) -> AccountOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    updates = []
                    values = []

                    if account.email is not None:
                        updates.append("email = %s")
                        values.append(account.email)

                    if account.image_href is not None:
                        updates.append("image_href = %s")
                        values.append(account.image_href)

                    values.append(account_id)

                    db.execute(
                        f"""
                        UPDATE accounts
                        SET {', '.join(updates)}
                        WHERE id = %s;
                        """,
                        values,
                    )

                    updated_account_data = self.get_account_by_id(account_id)
                    return updated_account_data

        except Exception as e:
            return {"message": e}

    def item_in_to_out(self, id: int, account: AccountUpdate):
        old_data = account.dict()
        return AccountOut(id=id, **old_data)
