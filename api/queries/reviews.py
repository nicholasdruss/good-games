from pydantic import BaseModel
from psycopg_pool import ConnectionPool
import os
from queries.games import GameOut
from fastapi import HTTPException

pool = ConnectionPool(conninfo=os.environ.get("DATABASE_URL"))


class Error(BaseModel):
    message: str


class ReviewIn(BaseModel):
    game_id: int
    user_id: int
    username: str
    recommended: bool
    details: str


class ReviewOut(BaseModel):
    id: int
    game_id: int
    user_id: int
    username: str
    recommended: bool
    details: str


class ReviewOutWithGame(ReviewOut):
    game: GameOut


class ReviewQueries:
    def add_review(
        self,
        game_id: int,
        user_id: int,
        username: str,
        recommended: bool,
        details: str,
    ) -> ReviewOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        INSERT INTO reviews
                            (
                            game_id,
                            user_id,
                            username,
                            recommended,
                            details
                            )
                        VALUES
                        (%s, %s, %s, %s, %s)
                        RETURNING
                        id,
                        game_id,
                        user_id,
                        username,
                        recommended,
                        details
                        """,
                        [game_id, user_id, username, recommended, details],
                    )
                    record = db.fetchone()
                    review = ReviewOut(
                        id=record[0],
                        game_id=record[1],
                        user_id=record[2],
                        username=record[3],
                        recommended=record[4],
                        details=record[5],
                    )
                    return review
        except Exception as e:
            return {"message": "Could not create review", "error": e}

    def get_reviews(self, game_id: int) -> list[ReviewOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT
                        id,
                        game_id,
                        user_id,
                        username,
                        recommended,
                        details
                        FROM reviews
                        WHERE game_id = %s
                        """,
                        [game_id],
                    )
                    records = db.fetchall()
                    return [
                        ReviewOut(
                            id=record[0],
                            game_id=record[1],
                            user_id=record[2],
                            username=record[3],
                            recommended=record[4],
                            details=record[5],
                        )
                        for record in records
                    ]
        except Exception as e:
            return {
                "message": "Could not retrieve reviews by specified game_id",
                "error": e,
            }

    def get_all_reviews(self) -> list[ReviewOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT
                        id,
                        game_id,
                        user_id,
                        username,
                        recommended,
                        details
                        FROM reviews
                        """
                    )
                    records = db.fetchall()
                    return [
                        ReviewOut(
                            id=record[0],
                            game_id=record[1],
                            user_id=record[2],
                            username=record[3],
                            recommended=record[4],
                            details=record[5],
                        )
                        for record in records
                    ]
        except Exception as e:
            raise HTTPException(
                status_code=500,
                detail="Could not retrieve reviews for specified game id",
                error=e,
            )

    def get_reviews_by_user(self, user_id: int) -> list[ReviewOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT
                        id,
                        game_id,
                        user_id,
                        username,
                        recommended,
                        details
                        FROM reviews
                        WHERE user_id = %s
                        """,
                        [user_id],
                    )
                    records = db.fetchall()
                    return [
                        ReviewOut(
                            id=record[0],
                            game_id=record[1],
                            user_id=record[2],
                            username=record[3],
                            recommended=record[4],
                            details=record[5],
                        )
                        for record in records
                    ]
        except Exception as e:
            return {"message": "Could not retrieve user reviews", "error": e}

    def get_reviews_by_user_with_game(
        self, user_id: int
    ) -> list[ReviewOutWithGame]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT
                        r.id,
                        r.game_id,
                        r.user_id,
                        r.username,
                        r.recommended,
                        r.details,
                        g.id,
                        g.game_name,
                        g.image_href
                        FROM reviews r
                        LEFT JOIN games g ON r.game_id = g.game_id
                        WHERE r.user_id = %s
                        """,
                        [user_id],
                    )
                    records = db.fetchall()
                    return [
                        ReviewOutWithGame(
                            id=record[0],
                            game_id=record[1],
                            user_id=record[2],
                            username=record[3],
                            recommended=record[4],
                            details=record[5],
                            game=GameOut(
                                id=record[6],
                                game_id=record[1],
                                game_name=record[7],
                                image_href=record[8],
                            ),
                        )
                        for record in records
                    ]
        except Exception as e:
            return {"message": "Could not retrieve user reviews", "error": e}

    def delete_review(self, id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM reviews
                        WHERE id = %s
                        """,
                        [id],
                    )
                    return db.rowcount > 0
        except Exception as e:
            return {"message": "Could not delete review", "error": e}

    def get_game_by_id(self, game_id: int):
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT * FROM games
                        WHERE id = %s
                        """,
                        [game_id],
                    )
                    return db.fetchone()
        except Exception as e:
            return {"message": "Could not retrieve specified game", "error": e}
