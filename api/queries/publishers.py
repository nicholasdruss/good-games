from pydantic import BaseModel
from psycopg_pool import ConnectionPool
import os

pool = ConnectionPool(conninfo=os.environ.get("DATABASE_URL"))


class DuplicateAccountError(ValueError):
    pass


class PublisherIn(BaseModel):
    publisher_id: int
    publisher_name: str


class PublisherOut(BaseModel):
    id: int
    publisher_id: int
    publisher_name: str


class PublisherQueries:
    def get_publisher(self, publisher_id) -> PublisherOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id, publisher_id, publisher_name
                        FROM publishers
                        WHERE publisher_id = %s;
                        """,
                        [publisher_id],
                    )
                    record = result.fetchone()
                    if record is None:
                        return None
                    return PublisherOut(
                        id=record[0],
                        publisher_id=record[1],
                        publisher_name=record[2],
                    )
        except Exception as e:
            return {"message": "Could not retrieve publisher", "error": e}
