from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
import os
from authenticator import authenticator
from routers import (
    accounts,
    games,
    platforms,
    publishers,
    reviews,
    details,
)


app = FastAPI()
app.include_router(authenticator.router)
app.include_router(accounts.router)
app.include_router(games.router, tags=["games"])
app.include_router(platforms.router, tags=["platforms"])
app.include_router(publishers.router, tags=["publisher"])
app.include_router(reviews.router, tags=["review"])
app.include_router(details.router, tags=["detail"])

app.add_middleware(
    CORSMiddleware,
    allow_origins=[
        os.environ.get(
            "CORS_HOST",
            "http://localhost:3000",
        )
    ],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


""" @app.get("/api/launch-details")
def launch_details():
    return {
        "launch_details": {
            "module": 3,
            "week": 17,
            "day": 5,
            "hour": 19,
            "min": "00"
        }
    } """
