steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE users (
            id SERIAL NOT NULL UNIQUE,
            email VARCHAR(255) NOT NULL UNIQUE,
            username VARCHAR(255) NOT NULL UNIQUE,
            display_name VARCHAR(255),
            hashed_password VARCHAR(255) NOT NULL,
            account_created DATE NOT NULL DEFAULT CURRENT_DATE
        );
        """,
        """
        DROP TABLE users;
        """,
    ]
]
