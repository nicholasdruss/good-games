steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE gameVO (
            id SERIAL NOT NULL UNIQUE,
            game_id INT NOT NULL,
            image_href VARCHAR(255),
            game_name VARCHAR(255)
        );
        """,
        """
        DROP TABLE gameVO;
        """,
    ]
]
