from main import app
from fastapi.testclient import TestClient
from queries.games import GameQueries


client = TestClient(app=app)


class EmptyGameReturn:
    def get_game(self, game_id):
        if game_id == 1:
            return {
                "id": 1,
                "game_id": 1,
                "game_name": "test",
                "image_href": "img",
            }
        else:
            return None


def test_get_game_from_db():
    app.dependency_overrides[GameQueries] = EmptyGameReturn

    id = 1

    response = client.get(f"/checkgame/{id}")

    assert response.status_code == 200

    assert response.json()["game_id"] == id
