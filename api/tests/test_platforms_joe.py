from main import app
from fastapi.testclient import TestClient
from queries.platforms import PlatformQueries

client = TestClient(app=app)


class EmptyPlatformReturn:
    def get_platform(self, platform_id):
        if platform_id == 1:
            return {
                "id": 1,
                "platform_id": 1,
                "platform_name": "test",
                "image_href": "img",
            }
        else:
            return None


def test_get_platform():
    app.dependency_overrides[PlatformQueries] = EmptyPlatformReturn

    id = 1

    response = client.get(f"/platform/{id}")
    print(response)
    assert response.status_code == 200

    assert response.json()["id"] == id
