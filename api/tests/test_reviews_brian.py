from main import app
from fastapi.testclient import TestClient
from queries.reviews import ReviewQueries
from pydantic import BaseModel

client = TestClient(app)


class ReviewOut(BaseModel):
    id: int
    game_id: int
    user_id: int
    username: str
    recommended: bool
    details: str


class EmptyReviewRepository(ReviewQueries):
    def get_all_reviews(self):
        return [
            ReviewOut(
                id=1,
                game_id=2,
                user_id=3,
                username="testname",
                recommended=True,
                details="game sucks but can't stop playing it",
            )
        ]

    def get_reviews(self, game_id: int):
        reviews = self.get_all_reviews()
        return [review for review in reviews if review.game_id == game_id]


def test_get_all_reviews():
    app.dependency_overrides[ReviewQueries] = EmptyReviewRepository

    response = client.get("/reviews")
    data = response.json()

    app.dependency_overrides = {}

    assert response.status_code == 200
    assert data[0]["id"] == 1
    assert data[0]["game_id"] == 2
    assert data[0]["user_id"] == 3
    assert data[0]["username"] == "testname"
    assert data[0]["recommended"] is True
    assert data[0]["details"] == "game sucks but can't stop playing it"
