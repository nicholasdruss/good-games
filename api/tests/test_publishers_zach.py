from main import app
from fastapi.testclient import TestClient
from queries.publishers import PublisherQueries

client = TestClient(app=app)


class EmptyPublisherReturn:
    def get_publisher(self, publisher_id):
        if publisher_id == 1:
            return {
                "id": 1,
                "publisher_id": 1,
                "name": "string"
            }
        else:
            return None


def test_get_publisher():
    app.dependency_overrides[PublisherQueries] = EmptyPublisherReturn

    response = client.get("/publishers")

    assert response.status_code == 200
