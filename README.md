# Good Games

Team:

- Brian Miller
- Nick Russ
- Joesian Lopez
- Zachary Ceniceros
- Samantha Hartig

</br>

## Table of Contents

[TOC]

## Design

Good Games is a video game review applicaiton.

</br>
</br>

## Instructions

**_How to Run_**

- Fork and clone the project

- `cd` into the newly cloned directory and run the following commands:

1. Run `docker volume create db`
2. Run `docker compose up`

- It may take around 5 minutes for the react development server to be fully functional at `http://localhost:3000`
- Use Navigation links at the top of the webpage to maneuver around the application
- Swagger Docs is a great resource to access the backend RESTful APIs at `http://localhost:8000/docs#/`

**_Troubleshooting Database_**

You may need to redo your database. You can follow these steps if needed:

1. Stop all services
2. Run `docker container prune -f`
3. Run `docker volume rm db`
4. Run `docker volume create db`
5. Run `docker compose up`

## Diagram

![image](/images/GoodGamesWireframe.png/)

</br>
</br>

## API endpoints

**_GAMES_**

| Action          | Method | URL `http://localhost:8000`                |
| --------------- | :----: | :----------------------------------------- |
| List genres     |  GET   | `/genres`                                  |
| List tags       |  GET   | `/tags`                                    |
| List orderings  |  GET   | `/orderings`                               |
| List games      |  GET   | `/games?page={pg id}`                      |
| Get game by id  |  GET   | `/checkgame/{game id}`                     |
| Search          |  GET   | `/search/{...}?page={...}&page_size={...}` |
| Details         |  GET   | `/game/{game id}`                          |
| Save game to db |  POST  | `/game/add`                                |

**_example genres_**

```json
[
  "Action",
  "Indie",
  "Adventure",
  "RPG",
  "Strategy",
  "Shooter",
  "Casual",
  "Simulation",
  "Puzzle",
  "Arcade",
  "Platformer",
  "Massively Multiplayer",
  "Racing",
  "Sports",
  "Fighting",
  "Family",
  "Board Games",
  "Educational",
  "Card"
]
```

**_example orderings_**

```json
["name", "released", "added", "created", "updated", "rating", "metacritic"]
```

**_example games_**

```json
[
  {
    "id": 3498,
    "game_name": "Grand Theft Auto V",
    "image_href": "https://media.rawg.io/media/games/20a/20aa03a10cda45239fe22d035c0ebe64.jpg"
  },
  {
    "id": 3328,
    "game_name": "The Witcher 3: Wild Hunt",
    "image_href": "https://media.rawg.io/media/games/618/618c2031a07bbff6b4f611f10b6bcdbc.jpg"
  }
]
```

**_example game by id_**

```json
{
  "id": 1,
  "game_id": 4200,
  "game_name": "Portal 2",
  "image_href": "https://media.rawg.io/media/games/2ba/2bac0e87cf45e5b508f227d281c9252a.jpg"
}
```

**_example search_**

```json
[
  {
    "id": 164830,
    "game_id": 164830,
    "game_name": "mario",
    "image_href": "https://media.rawg.io/media/screenshots/1d7/1d75b9d60cb5884a0b19d21df8557c0c.jpg"
  }
]
```

**_example add game in_**

```json
{
  "game_id": 0,
  "game_name": "string",
  "image_href": "string"
}
```

**_example add game out_**

```json
{
  "id": 0,
  "game_id": 0,
  "game_name": "string",
  "image_href": "string"
}
```

**_example details_**

```json
{
  "id": 4200,
  "game_name": "Portal 2",
  "details": "<p>Portal 2 is a first-person puzzle game developed by Valve Corporation and released on April 19, 2011 on Steam, PS3 and Xbox 360. It was published by Valve Corporation in digital form and by Electronic Arts in physical form. </p>\n<p>Its plot directly follows the first game&#39;s, taking place in the Half-Life universe. You play as Chell, a test subject in a research facility formerly ran by the company Aperture Science, but taken over by an evil AI that turned upon its creators, GladOS. After defeating GladOS at the end of the first game but failing to escape the facility, Chell is woken up from a stasis chamber by an AI personality core, Wheatley, as the unkempt complex is falling apart. As the two attempt to navigate through the ruins and escape, they stumble upon GladOS, and accidentally re-activate her...</p>\n<p>Portal 2&#39;s core mechanics are very similar to the first game&#39;s ; the player must make their way through several test chambers which involve puzzles. For this purpose, they possess a Portal Gun, a weapon capable of creating teleportation portals on white surfaces. This seemingly simple mechanic and its subtleties coupled with the many different puzzle elements that can appear in puzzles allows the game to be easy to start playing, yet still feature profound gameplay. The sequel adds several new puzzle elements, such as gel that can render surfaces bouncy or allow you to accelerate when running on them.</p>\n<p>The game is often praised for its gameplay, its memorable dialogue and writing and its aesthetic. Both games in the series are responsible for inspiring most puzzle games succeeding them, particularly first-person puzzle games. The series, its characters and even its items such as the portal gun and the companion cube have become a cultural icon within gaming communities.</p>\n<p>Portal 2 also features a co-op mode where two players take on the roles of robots being led through tests by GladOS, as well as an in-depth level editor.</p>",
  "image_href": "https://media.rawg.io/media/games/2ba/2bac0e87cf45e5b508f227d281c9252a.jpg",
  "release_date": "2011-04-18",
  "platforms": []
}
```

</br>
</br>

**_PLATFORMS_**

| Action         | Method | URL `http://localhost:8000` |
| -------------- | :----: | :-------------------------- |
| List platforms |  GET   | `/platform`                 |
| Platform by id |  GET   | `platform/{plat id}`        |

**_example list platforms_**

```json
[
  {
    "id": 4,
    "platform_name": "PC",
    "image_href": "https://media.rawg.io/media/games/bc0/bc06a29ceac58652b684deefe7d56099.jpg"
  },
  {
    "id": 187,
    "platform_name": "PlayStation 5",
    "image_href": "https://media.rawg.io/media/games/909/909974d1c7863c2027241e265fe7011f.jpg"
  }
]
```

**_example platform_**

```json
{
  "id": 4,
  "platform_name": "PC",
  "image_href": "https://media.rawg.io/media/games/bc0/bc06a29ceac58652b684deefe7d56099.jpg",
  "games_count": 516277,
  "image": null,
  "description": "<p>PC games, or personal computer games, started with the video game crash of 1983. PC games became popular after the development of the microprocessor and microcomputer. Some of the first PC games were Bertie the Brain, OXO and Spacewar!<br />\nAs the 3D graphics accelerators became faster and CPU power improved, PC games became more realistic and more accessible to produce. The PC market sales rocketed in the 80s when IBM computers and sound cards were generated. The platform involves different peripherals, gaming hardware, and software. These are mouse and keyboard; gamepads and motion controllers aren&#39;t obligatory, but still popularly accepted. Better hardware improves the game&#39;s accuracy; it usually lets the players use more NPCs than equivalents on other platforms. With the platform, the players can perform every sort of game. For example, shooters are easy to play due to the mouse controllers. However, the main reason for the PC games popularity is their lower prices and the backward compatibility with older titles, which leaves much to be desired on cosoles.</p>",
  "year_start": null,
  "year_end": null
}
```

</br>
</br>

**_PUBLISHERS_**

| Action          | Method | URL `http://localhost:8000` |
| --------------- | :----: | :-------------------------- |
| List publishers |  GET   | `/publishers`               |
| Platform by id  |  GET   | `/publisher/{pub id}`       |

**_example list publishers_**

```json
[
  {
    "id": 354,
    "name": "Electronic Arts",
    "image_href": "https://media.rawg.io/media/games/260/26023c855f1769a93411d6a7ea084632.jpeg"
  },
  {
    "id": 308,
    "name": "Square Enix",
    "image_href": "https://media.rawg.io/media/games/410/41033a495ce8f7fd4b0934bdb975f12a.jpg"
  }
]
```

**_example publisher_**

```json
{
  "id": 354,
  "name": "Electronic Arts",
  "games_count": 1310,
  "image_href": "https://media.rawg.io/media/games/260/26023c855f1769a93411d6a7ea084632.jpeg",
  "description": "<p>Electronic Arts is an American video game publisher based in Redwood City, California.</p>\n<h3>History</h3>\n<p>The company was created by Trip Hawkins on May 28th, 1982 by Trip Hawkins and became one of the first corporations in the video game industry. It was also the first company to popularize its designers and programmers.</p>\n<h3>Main series</h3>\n<p>Electronic Arts has multiple game series in development, featuring a broad range of genres. The company focuses on sports with its FIFA, NBA Live, NHL, and UFC titles, offers a variety of racing games with its Need For Speed series, and military shooters, that is being the Battlefield franchise. </p>\n<h3>Online distribution</h3>\n<p>At the beginning of 2011, EA released Origin, its own online marketplace. It allows for purchasing and downloading EA titles online, much to how Steam operates. Origin is integrated with Facebook, XBL, and PSN.</p>"
}
```

</br>
</br>

**_REVIEWS_**

| Action             | Method | URL `http://localhost:8000` |
| ------------------ | :----: | :-------------------------- |
| Add reviews        |  POST  | `/reviews`                  |
| List reviews       |  GET   | `/reviews`                  |
| Reviews by user id |  GET   | `/reviews/user/{user id}`   |
| Reviews by game id |  GET   | `/reviews/game/{game id}`   |
| Delete review      | DELETE | `/reviews/{rev id}`         |

**_example add review_**

```json
{
  "id": 0,
  "game_id": 0,
  "user_id": 0,
  "username": "string",
  "recommended": true,
  "details": "string"
}
```

**_example list reviews_**

```json
[
  {
    "id": 2,
    "game_id": 11859,
    "user_id": 1,
    "username": "test22",
    "recommended": true,
    "details": "test review"
  },
  {
    "id": 3,
    "game_id": 4459,
    "user_id": 1,
    "username": "test22",
    "recommended": false,
    "details": "test review"
  }
]
```

**_example list reviews by user_**

```json
[
  {
    "id": 2,
    "game_id": 11859,
    "user_id": 1,
    "username": "test22",
    "recommended": true,
    "details": "test review",
    "game": {
      "id": 1,
      "game_id": 11859,
      "game_name": "Team Fortress 2",
      "image_href": "https://media.rawg.io/media/games/46d/46d98e6910fbc0706e2948a7cc9b10c5.jpg"
    }
  },
  {
    "id": 3,
    "game_id": 4459,
    "user_id": 1,
    "username": "test22",
    "recommended": false,
    "details": "test review",
    "game": {
      "id": 2,
      "game_id": 4459,
      "game_name": "Grand Theft Auto IV",
      "image_href": "https://media.rawg.io/media/games/4a0/4a0a1316102366260e6f38fd2a9cfdce.jpg"
    }
  }
]
```

**_example list reviews by game_**

```json
[
  {
    "id": 3,
    "game_id": 4459,
    "user_id": 1,
    "username": "test22",
    "recommended": false,
    "details": "test review"
  }
]
```

</br>
</br>

**_ACCOUNTS_**

| Action         | Method | URL `http://localhost:8000` |
| -------------- | :----: | :-------------------------- |
| Create account |  POST  | `/accounts`                 |
| Login          |  POST  | `/token`                    |
| Token          |  GET   | `/token`                    |
| Logout         | DELETE | `/token`                    |
| Get account id |  GET   | `/reviews/game/{game id}`   |
| Update account |  PUT   | `/reviews/{rev id}`         |

**_example create account in_**

```json
{
  "email": "string@email.com",
  "username": "test22",
  "image_href": "string",
  "password": "test"
}
```

**_example create account out_**

```json
{
  "access_token": "eyJhbGciOiJI",
  "token_type": "Bearer",
  "account": {
    "id": 1,
    "email": "string@email.com",
    "username": "test22",
    "image_href": "string"
  }
}
```

**_example login out_**

```json
{
  "access_token": "eyJhbGciOiJI",
  "token_type": "Bearer"
}
```

**_example get token_**

```json
{
  "access_token": "string",
  "token_type": "Bearer",
  "account": {
    "id": 0,
    "email": "string",
    "username": "string",
    "image_href": "string"
  }
}
```

**_example get account out_**

```json
{
  "id": 0,
  "email": "string",
  "username": "string",
  "image_href": "string"
}
```

**_example update account_**

```json
{
  "email": "update@email.com",
  "image_href": "update"
}
```

</br>
</br>
